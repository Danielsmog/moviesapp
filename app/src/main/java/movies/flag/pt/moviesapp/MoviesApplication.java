package movies.flag.pt.moviesapp;

import android.app.Application;

import movies.flag.pt.moviesapp.helpers.SharedPreferencesHelper;

/**
 * Created by Formando on 30/06/2017.
 */

    public class MoviesApplication extends Application {

    @Override
        public void onCreate() {
            super.onCreate();
            SharedPreferencesHelper.init(this);
    }
}
