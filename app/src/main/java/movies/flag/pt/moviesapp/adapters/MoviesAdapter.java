package movies.flag.pt.moviesapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.helpers.DownloadsHelper;
import movies.flag.pt.moviesapp.helpers.ImagesHelper;
import movies.flag.pt.moviesapp.http.entities.Movie;
import movies.flag.pt.moviesapp.screens.DetailsScreen;
import movies.flag.pt.moviesapp.utils.DLog;


/**
 * Created by Daniel Gomes on 08/06/2017.
 */

public class MoviesAdapter extends ArrayAdapter<Movie> {

    public MoviesAdapter(Context context, ArrayList<Movie> list) {
        super(context, 0, list);
    }

    @NonNull
    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View v;
        final ViewHolder viewHolder;
        final Movie movie = getItem(position);

        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.movies, null);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) v.findViewById(R.id.movie_title);
            viewHolder.imageView = (ImageView) v.findViewById(R.id.download_image_screen_image);
            viewHolder.popularity = (TextView) v.findViewById(R.id.movie_popularity);
            viewHolder.linearLayout = (LinearLayout) v.findViewById(R.id.movie_adapter_item);


            v.setTag(viewHolder);
        } else {
            v = convertView;
            viewHolder = (ViewHolder) v.getTag();
        }

        DownloadImageAsyncTask downloadImageAsyncTask = new DownloadImageAsyncTask(viewHolder);
        downloadImageAsyncTask.execute(getContext().getString(R.string.server_image_path_)+ movie.getPosterPath());



        //viewHolder.title.setTextColor(Color.BLACK);
        viewHolder.title.setText(String.valueOf(movie.getTitle()));

        //viewHolder.popularity.setTextColor(Color.BLACK);
        viewHolder.popularity.setText(getContext().getString(R.string.popularity, movie.getPopularity()));


        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetailsScreen.class);


                String textOriginalLanguage = movie.getOriginalLanguage();
                intent.putExtra(DetailsScreen.ORIGINAL_LANGUAGE_EXTRA_KEY, textOriginalLanguage);

                String textOverview = movie.getOverview();
                intent.putExtra(DetailsScreen.OVERVIEW_EXTRA_KEY, textOverview);

                String textReleaseDate = movie.getReleaseDate();
                intent.putExtra(DetailsScreen.RELEASE_DATE_EXTRA_KEY, textReleaseDate);

                String textTitle = movie.getOriginalTitle();
                intent.putExtra(DetailsScreen.ORIGINAL_MOVIE_TITLE_EXTRA_KEY, textTitle);

                getContext().startActivity(intent);
            }
        });


        //notifyDataSetChanged();



        return v;

    }


    public class ViewHolder{
        public TextView title;
        public ImageView imageView;
        public TextView popularity;
        public LinearLayout linearLayout;
    }




    public class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

        private final String TAG = DownloadImageAsyncTask.class.getSimpleName();

        private ViewHolder viewHolder;

        public DownloadImageAsyncTask(ViewHolder viewHolder) {
            this.viewHolder = viewHolder;
        }

        @Override
        protected void onPreExecute() {
            DLog.d(TAG, "onPreExecute");
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            DLog.d(TAG, "doInBackground");

            String url = params[0];

            return ImagesHelper.convertByteArrayToBitmap(DownloadsHelper.downloadImageUrl(url));

        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            DLog.d(TAG, "onPostExecute");

            viewHolder.imageView.setImageBitmap(bitmap);

        }
    }
}




