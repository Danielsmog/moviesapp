package movies.flag.pt.moviesapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.helpers.DownloadsHelper;
import movies.flag.pt.moviesapp.helpers.ImagesHelper;
import movies.flag.pt.moviesapp.http.entities.Serie;
import movies.flag.pt.moviesapp.utils.DLog;

/**
 * Created by Daniel Gomes on 06/07/2017.
 */

public class SeriesAdapter extends ArrayAdapter<Serie> {

    public SeriesAdapter(Context context, ArrayList<Serie> list) {
        super(context, 0, list);
    }

    @NonNull
    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        View v;
        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.series, null);

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) v.findViewById(R.id.serie_title);
            viewHolder.imageView = (ImageView) v.findViewById(R.id.downloads_image_screen_image);


            v.setTag(viewHolder);
        } else {
            v = convertView;
            viewHolder = (ViewHolder) v.getTag();
        }

        final Serie serie = getItem(position);


        viewHolder.title.setText(String.valueOf(serie.getName()));







        //notifyDataSetChanged();

        DownloadImageAsyncTask downloadImageAsyncTask = new DownloadImageAsyncTask(viewHolder);
        downloadImageAsyncTask.execute(getContext().getString(R.string.server_image_path_)+ serie.getPosterPath());

        return v;
    }




    public class ViewHolder{
        public TextView title;
        public ImageView imageView;
    }



    public class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

        private final String TAG = DownloadImageAsyncTask.class.getSimpleName();

        private ViewHolder viewHolder;

        public DownloadImageAsyncTask(ViewHolder viewHolder) {
            this.viewHolder = viewHolder;
        }

        @Override
        protected void onPreExecute() {
            DLog.d(TAG, "onPreExecute");
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            DLog.d(TAG, "doInBackground");

            String url = params[0];

            return ImagesHelper.convertByteArrayToBitmap(DownloadsHelper.downloadImageUrl(url));
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            DLog.d(TAG, "onPostExecute");
            viewHolder.imageView.setImageBitmap(bitmap);
        }

    }
}




