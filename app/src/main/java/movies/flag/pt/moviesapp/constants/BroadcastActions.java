package movies.flag.pt.moviesapp.constants;

/**
 * Created by Daniel Gomes on 13/07/2017.
 */
public final class BroadcastActions {

    public static final String BROADCAST_IMAGE_ACTION = "broadcast_image_action";
    public static final String NEW_IMAGES_DOWNLOADED_ACTION = "new_images_downloaded_action";

}