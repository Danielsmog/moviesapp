package movies.flag.pt.moviesapp.constants;

/**
 * Created by Ricardo Neves on 10/01/2017.
 */

public final class BroadcastExtras {
    public static final String BITMAP_KEY_EXTRA = "bitmap_key_extra";
}
