package movies.flag.pt.moviesapp.constants;

/**
 * Created by Daniel Gomes on 13/07/2017.
 */
public final class PreferenceIds {

    public static final String PREFERENCES_FILE_NAME = "FlagPreferences";
    public static final String CURRENT_IMAGE_INDEX = "CurrentImageIndex";
    public static final String IS_FIRST_USE = "IsFirstUse";

}
