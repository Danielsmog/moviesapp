package movies.flag.pt.moviesapp.helpers;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Ricardo Neves on 02/12/2014.
 */
public final class DownloadsHelper {

    private static final String TAG = DownloadsHelper.class.getSimpleName();

    public static final byte[] downloadImageUrl(String url){
        try {
            InputStream is = new java.net.URL(url).openStream();

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[4096]; // buffer size

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
            is.close();

            Log.e(TAG, String.format("Download efectuado com sucesso do url = %s", url));

            return buffer.toByteArray();
        }
        catch(IOException e){
            Log.e(TAG, "Excepção: " + e.getMessage());
        }

        return null;
    }

}
