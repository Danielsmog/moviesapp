package movies.flag.pt.moviesapp.http.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Formando on 23/06/2017.
 */

public class Serie {

    @SerializedName("original_name")
    private String originalName;


    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("poster_path")
    @Expose
    private String posterPath;

    @SerializedName("name")
    @Expose
    private String name;


    /**
     *
     * @return
     *     The title
     */
    public String getName() {
        return name;
    }


    /**
     *
     * @return
     *     The posterPath
     */
    public String getPosterPath() {
        return posterPath;
    }

}
