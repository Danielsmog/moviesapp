package movies.flag.pt.moviesapp.http.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Formando on 23/06/2017.
 */

public class SeriesResponse {
    @SerializedName("results")
    private ArrayList<Serie> series;

    public ArrayList<Serie> getSeries() {
        return series;
    }
}
