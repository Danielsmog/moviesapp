package movies.flag.pt.moviesapp.http.requests;

import android.content.Context;

import movies.flag.pt.moviesapp.http.entities.SeriesResponse;

/**
 * Created by Formando on 23/06/2017.
 */

public abstract class GetPopularSeriesAsyncTask extends ExecuteRequestAsyncTask<SeriesResponse>{


    private static final String PAGE_KEY = "page";


    private String page;


    public GetPopularSeriesAsyncTask(Context context, int page) {
        super(context);
        this.page = String.valueOf(page);
    }


    @Override
    protected String getPath() {
        return "/tv/popular";
    }

    @Override
    protected void addQueryParams(StringBuilder sb) {
        addQueryParam(sb, "language", "pt");
        addQueryParam(sb, PAGE_KEY, page);
    }

    @Override
    protected Class<SeriesResponse> getResponseEntityClass() {
        return SeriesResponse.class;
    }

}
