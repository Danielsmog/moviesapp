package movies.flag.pt.moviesapp.http.requests;

import android.content.Context;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.http.entities.MoviesResponse;

/**
 * Created by Formando on 28/06/2017.
 */

public abstract class SearchAsyncTask extends ExecuteRequestAsyncTask<MoviesResponse>  {

    private String query;
    private static final String PATH = "/search/movie";
    private static final String LANGUAGE_KEY = "language";


    public SearchAsyncTask(Context context, String query) {
        super(context);
        this.query = query;
    }

    @Override
    protected String getPath() {
        return PATH;
    }

    @Override
    protected void addQueryParams(StringBuilder sb) {
        addQueryParam(sb, "query", query);
        addQueryParam(sb, LANGUAGE_KEY, context.getString(R.string.language));

    }

    @Override
    protected Class<MoviesResponse> getResponseEntityClass() {
        return MoviesResponse.class;
    }
}
