package movies.flag.pt.moviesapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import movies.flag.pt.moviesapp.helpers.NetworkHelper;
import movies.flag.pt.moviesapp.utils.DLog;

/**
 * Created by Daniel Gomes on 13/07/2017.
 */

    public class NetworkChangeBroadcastReceiver extends BroadcastReceiver {

        private static final String TAG = NetworkChangeBroadcastReceiver.class.getSimpleName();


        @Override
        public void onReceive(Context context, Intent intent) {
            if(NetworkHelper.networkIsAvailable(context)){
                DLog.d(TAG, "wifi is available");
                DLog.d(TAG, "Start VerifyImagesToDownloadService");

            }
            else{
                DLog.d(TAG, "wifi is not available");
            }
        }

    }

