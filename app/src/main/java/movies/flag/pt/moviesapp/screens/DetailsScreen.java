package movies.flag.pt.moviesapp.screens;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import movies.flag.pt.moviesapp.R;

import static movies.flag.pt.moviesapp.R.layout.details_screen;
import static movies.flag.pt.moviesapp.R.string.originalLanguage;

/**
 * Created by Formando on 26/06/2017.
 */

public class DetailsScreen extends Screen{

    public static final String ORIGINAL_MOVIE_TITLE_EXTRA_KEY = "Title";
    public static final String ORIGINAL_LANGUAGE_EXTRA_KEY = "Language";
    public static final String OVERVIEW_EXTRA_KEY = "Overview";
    public static final String RELEASE_DATE_EXTRA_KEY = "ReleaseDate";


    private String textReceivedOverview;
    private String textReceivedTitle;

    private TextView title;
    private TextView language;
    private TextView overview;
    private TextView releaseDate;
    private Button openUrlButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViews();



        addListeners();
    }


    private void findViews() {
        setContentView(details_screen);


        title = (TextView) findViewById(R.id.details_title);
        //title.setTextColor(Color.BLACK);

        language = (TextView) findViewById(R.id.details_language);
        //language.setTextColor(Color.BLACK);

        overview = (TextView) findViewById(R.id.details_overview);
        //overview.setTextColor(Color.BLACK);

        releaseDate  = (TextView) findViewById(R.id.details_release_date);
        //releaseDate.setTextColor(Color.BLACK);

        openUrlButton = (Button) findViewById(R.id.open_url);



        Intent intent = getIntent();
        textReceivedOverview = intent.getStringExtra(OVERVIEW_EXTRA_KEY);
        String textReceivedLanguage = intent.getStringExtra(ORIGINAL_LANGUAGE_EXTRA_KEY);
        String textReceivedReleaseDate = intent.getStringExtra(RELEASE_DATE_EXTRA_KEY);
        textReceivedTitle = intent.getStringExtra(ORIGINAL_MOVIE_TITLE_EXTRA_KEY);



        title.setText(textReceivedTitle);
        language.setText(getString(originalLanguage, textReceivedLanguage));
        overview.setText(getString(R.string.overview, textReceivedOverview));
        releaseDate.setText(getString(R.string.releaseDate, textReceivedReleaseDate));
    }
    private void addListeners() {
        openUrlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StringBuilder sb = new StringBuilder();
                sb.append(textReceivedTitle).append(textReceivedOverview);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }

}




