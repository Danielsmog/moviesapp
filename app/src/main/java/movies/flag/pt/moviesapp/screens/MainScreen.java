package movies.flag.pt.moviesapp.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;

import movies.flag.pt.moviesapp.R;


/**
 * Created by Formando on 28/06/2017.
 */

public class MainScreen extends Screen {

    private Button moviesButton;
    private Button seriesButton;
    private FloatingActionButton searchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_screen);
        findViews();
        addListeners();

    }



    private void findViews() {
        moviesButton = (Button) findViewById(R.id.movies_screen);
        seriesButton = (Button) findViewById(R.id.series_screen);
        searchButton = (FloatingActionButton) findViewById(R.id.search_button);
    }


    private void addListeners() {
        moviesButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    startActivity(MoviesScreen.class);
                }
        });
        seriesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(SeriesScreen.class);
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(SearchScreen.class);
            }
        });
    }

    private void startActivity(Class<?> activityClass){
        Intent intent = new Intent(MainScreen.this, activityClass);
        startActivity(intent);
    }


}