package movies.flag.pt.moviesapp.screens;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.GridView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.adapters.MoviesAdapter;
import movies.flag.pt.moviesapp.helpers.NetworkHelper;
import movies.flag.pt.moviesapp.helpers.SharedPreferencesHelper;
import movies.flag.pt.moviesapp.http.entities.Movie;
import movies.flag.pt.moviesapp.http.entities.MoviesResponse;
import movies.flag.pt.moviesapp.http.requests.GetNowPlayingMoviesAsyncTask;
import movies.flag.pt.moviesapp.receiver.NetworkChangeBroadcastReceiver;
import movies.flag.pt.moviesapp.utils.DLog;

/**
 * Created by Ricardo Neves on 06/06/2017.
 */

public class MoviesScreen extends Screen implements SwipeRefreshLayout.OnRefreshListener {


    private GridView gridView;
    public SwipeRefreshLayout swipeRefresh;
    public static final int FIVE_MINUTES = 1000 * 60 * 5;
    private long currentTime = System.currentTimeMillis();
    private BroadcastReceiver update;
    //private TextView date;

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intent = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(update,intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_screen);

         update = new NetworkChangeBroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                long lastTime = System.currentTimeMillis() - FIVE_MINUTES ;


                SharedPreferencesHelper.savePreference(SharedPreferencesHelper.MOVIES_LIST_TIMESTAMP, currentTime);

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd/ss", Locale.getDefault());
                //String dateAsString = dateFormat.format(new Date(currentTime));
                //date.setText(String.valueOf(dateAsString));


                if ( NetworkHelper.networkIsAvailable(context) && lastTime - currentTime > FIVE_MINUTES ) {
                executeRequest(1);
                }
            }
        };




        findViews();

        addListeners();
        executeRequest(1);
    }

    private void executeRequest(int page) {
        // Example to request get now playing movies
        new GetNowPlayingMoviesAsyncTask(this, page){

            protected void onResponseSuccess(MoviesResponse moviesResponse) {
                DLog.d(tag, "onResponseSuccess " + moviesResponse);

                ArrayList<Movie> movies = moviesResponse.getMovies();

                MoviesAdapter adapter = new MoviesAdapter(MoviesScreen.this, movies);
                gridView.setAdapter(adapter);

                swipeRefresh.setRefreshing(false);
            }

            @Override
            protected void onNetworkError() {
                DLog.d(tag, "onNetworkError");
                // Here i now that some error occur when processing the request,
                // possible my internet connection if turned off
            }
        }.execute();
    }

    private void addListeners(){
        swipeRefresh.setOnRefreshListener(this);
    }

    private void findViews() {
        gridView = (GridView) findViewById(R.id.first_screen_grid_view);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        //date = (TextView) findViewById(R.id.first_screen_date);
    }

    @Override
    public void onRefresh() {
        Random r = new Random();
        int result = r.nextInt(50) + 1;

        executeRequest(result);
    }

    @Override
    protected void onDestroy() {
        super.onStop();
        unregisterReceiver(update);
    }

}
