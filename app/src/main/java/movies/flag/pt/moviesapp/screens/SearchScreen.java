package movies.flag.pt.moviesapp.screens;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

import java.util.ArrayList;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.adapters.MoviesAdapter;
import movies.flag.pt.moviesapp.http.entities.Movie;
import movies.flag.pt.moviesapp.http.entities.MoviesResponse;
import movies.flag.pt.moviesapp.http.requests.SearchAsyncTask;


/**
 * Created by danielgomes on 29/06/17.
 */
//<!-- -->
public class SearchScreen extends Screen {
    private Button searchButton;
    private EditText inputText;
    private GridView grid;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cleanInput();
        setContentView(R.layout.search_screen);
        findViews();
        addListeners();




        //executeRequest(query);
    }

    private void cleanInput() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void findViews() {
        searchButton = (Button) findViewById(R.id.search_button_search_screen);
        inputText = (EditText) findViewById(R.id.input_text_search_screen);
        grid = (GridView) findViewById(R.id.grid_search_screen);

    }


    private void executeRequest(String query) {

        new SearchAsyncTask(this, query) {

            protected void onResponseSuccess(MoviesResponse moviesResponse) {

                ArrayList<Movie> movies = moviesResponse.getMovies();
                MoviesAdapter adapter = new MoviesAdapter(SearchScreen.this, movies);
                grid.setAdapter(adapter);


            }


            protected void onNetworkError() {

            }
        }.execute();


    }


    private void addListeners() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                String searchText = inputText.getText().toString();

                if(searchText.equals("")) {
                    return;
                } else {
                    executeRequest(searchText);
                }
                try  {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }
            }
        });
    }
}






