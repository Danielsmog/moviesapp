package movies.flag.pt.moviesapp.screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.GridView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.adapters.SeriesAdapter;
import movies.flag.pt.moviesapp.helpers.SharedPreferencesHelper;
import movies.flag.pt.moviesapp.http.entities.Serie;
import movies.flag.pt.moviesapp.http.entities.SeriesResponse;
import movies.flag.pt.moviesapp.http.requests.GetPopularSeriesAsyncTask;
import movies.flag.pt.moviesapp.utils.DLog;

/**
 * Created by Daniel Gomes on 06/07/2017.
 */



    public class SeriesScreen extends Screen implements SwipeRefreshLayout.OnRefreshListener {


        private GridView gridView;
        public SwipeRefreshLayout swipeRefresh;
        public static final int FIVE_MINUTES = 1000 * 60 * 5;


        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setContentView(R.layout.series_screen);


            findViews();
            addListeners();




            executeRequest(1);
        }

        private void executeRequest(int page) {
            // Example to request get now playing movies
            new GetPopularSeriesAsyncTask(this, page) {

                protected void onResponseSuccess(SeriesResponse seriesResponse) {
                    DLog.d(tag, "onResponseSuccess " + seriesResponse);


                    long lastTime = 0;
                    long currentTime = System.currentTimeMillis();

                    if (currentTime - lastTime > FIVE_MINUTES) {

                    }
                    // guardar share preferenrces


                    SharedPreferencesHelper.savePreference(SharedPreferencesHelper.SERIES_LIST_TIMESTAMP, currentTime);

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                    String dateAsString = dateFormat.format(new Date(currentTime));


                    ArrayList<Serie> series = seriesResponse.getSeries();

                    SeriesAdapter adapter = new SeriesAdapter(SeriesScreen.this, series);
                    gridView.setAdapter(adapter);

                    swipeRefresh.setRefreshing(false);

                }

                @Override
                protected void onNetworkError() {
                    DLog.d(tag, "onNetworkError ");
                    // Here i now that some error occur when processing the request,
                    // possible my internet connection if turned off
                }
            }.execute();

        }


        private void addListeners(){
            swipeRefresh.setOnRefreshListener(this);

        }

        private void findViews() {
            gridView = (GridView) findViewById(R.id.series_screen_grid_view);
            swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        }

        @Override
        public void onRefresh() {
            Random r = new Random();
            int result = r.nextInt(50) + 1;

            executeRequest(result);
        }
    }

