package movies.flag.pt.moviesapp.screens;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import movies.flag.pt.moviesapp.R;
import movies.flag.pt.moviesapp.constants.PreferenceIds;
import movies.flag.pt.moviesapp.helpers.SharedPreferencesHelper;


/**
 * Created by Daniel Gomes on 08/06/2017.
 */
public class SplashScreen extends Screen{

    private ImageView splashImageView;
    private RelativeLayout rootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);

        findViews();
        addListeners();

        verifyFirstUse();
    }
    private void findViews(){
        splashImageView = (ImageView) findViewById(R.id.splash_screen_image);
        rootView = (RelativeLayout) findViewById(R.id.splash_screen_root);
    }



    private void addListeners() {


            Animation anim = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
            splashImageView.setAnimation(anim);


            Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    Intent intent = new Intent(SplashScreen.this, MainScreen.class);
                    startActivity(intent);
                }
            }, 1500);
        }

    private void verifyFirstUse() {
        boolean firstUse = false;
        if(SharedPreferencesHelper.getStringPreference(PreferenceIds.IS_FIRST_USE) == null){
            firstUse = true;
            SharedPreferencesHelper.savePreference(PreferenceIds.IS_FIRST_USE, "");
        }

        Snackbar.make(rootView, getString(R.string.first_time, firstUse),
                Snackbar.LENGTH_SHORT).show();
    }
}


